#Welcome Web_TYHJ

###项目背景

    项目是为北京天乙鸿基有限公司，设计的一款专门用于库存管理的系统软件。


###系统必备

    ThinkPHP >= 3.2.3
    PHP >= 5.3
    MySQL >= 5.6.12


###运行环境
   
    WAMPServer 2.4


###主要功能

    入库管理、出库管理、派货


###联系我

    hiccupzhu@163.com