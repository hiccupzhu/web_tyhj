<?php
namespace Admin\Controller;


class ProductController extends TableController {
    public $__tname__ = "product";
    
    function _initialize(){
        header("Content-type:text/html;charset=utf-8");
    }
    
    public function index(){
        $this->display();
    }
    
    
    public function Retrieve($tname=''){
        $tname = $this->__tname__ == "" ? $tname : $this->__tname__;
    
        $Table = M($tname);
    
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
        $offset = ($page-1)*$rows;
        $result = array();
    
        $total = $Table->count();
        $result["total"] = $total;
    
        if(isset($_REQUEST['where'])){
            //这是为了避免以%传参时，若后面是数字，解析时将发生转义,此时用*代替%
            $where['_string'] = str_replace("*", "%",$_REQUEST['where']);
            $rs = $Table->where($where)->limit($offset, $rows)->select();
        }else{
            $rs = $Table->limit($offset, $rows)->select();
        }
    
        //dump($rs);
        $result["rows"] = $rs;
    
        echo json_encode($result);
    }
    
    
    public function Update($id){
        unset($_POST["name"]);
    
        $num = I('post.ctnnum') * I('post.ctnper')  + I('post.fraction');
        $cost = $num * I('post.price');
        $_POST["cost"] = $cost;
    
    
        $Table = M("product");
        $result = $Table->where("id=$id")->save($_POST);
        if ($result) {
            echo json_encode ( array (
                    'success' => '更新成功',
                    cost => $cost
            ) );
        } else {
            echo json_encode ( array (
                    'msg' => "Failed:".$Table->getLastSql()
            ) );
        }
    }
    
    public function Delete($id){
        $Table = A("Table");
    
        $Table->Delete("istorage", $id);
    }
    
    
    public function Inc($id, $num){
        $ret = M("product")->where("id='$id'")->setInc("count", $num);
        
        if($ret == false){
            exit(json_encode(array(
            	fail => "***".M("product")->getError()
            )));
        }
    }
    
    
    public function Dec($id, $num){
        $ret = M("product")->where("id='$id'")->setDec("count", $num);
        
        if($ret == false){
            exit(json_encode(array(
                    fail => "***".M("product")->getError()
            )));
        }
    }
    
    
    
    public function getNameByID($id){
        $prod = M('product');
        
        $rows = $prod->where("id='$id'")->select();
        $row = $rows[0];
        
        if($row){
            exit(json_encode(array(
                status => true,
            	'msg'  => "Successfule",
                'name' => $row['name']
            )));
        }else{
            exit(json_encode(array(
                status => false,
                'msg'  => "FAILED"
            )));
        }
    }
}