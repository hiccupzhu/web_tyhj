<?php
namespace Admin\Controller;


class OStorageController extends TableController {

    public $__tname__ = "ostorage";
    
    public function Create(){
        //箱数*每箱个数+散件数
        $num = I('post.ctnnum') * I('post.ctnper')  + I('post.fraction');
        A("product")->Dec(I('post.pid'), $num);
        
        $Table = M($this->__tname__);
        
        $cost = (I('post.ctnnum') * I('post.ctnper')  + I('post.fraction')) * I('post.price');
        $_POST['cost'] = $cost;
        $_POST['createdate'] = date("Y-m-d H:i:s",time());
        
        
        $result = $Table->create($_POST);
        if ($result && $Table->add()) {
            echo json_encode ( array (
                    'success' => 'SAVE successful.',
                    cost => $cost
            ) );
            exit();
        }
        
        echo json_encode ( array (
                'msg' => mysql_error ()
        ) );
    }
   
    
    public function Retrieve(){
//         $tname = "ostorage o";
        //下面的o是ostorage的一个别名，用在查询的时候
        $Table = M($this->__tname__." o");
        
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
        $offset = ($page-1)*$rows;
        $result = array();
        
        $total = $Table->count();
        
        
        
        $result["total"] = $total;
        
        $rs = $Table->join("t_product p on o.pid=p.id")->field("o.*,p.name")->limit($offset, $rows)->select();
        
        //dump($rs);
        $result["rows"] = $rs;
        
        echo json_encode($result);
    }
    
    
    
    public function Update($id){
        $Table = M($this->__tname__);
        
        unset($_POST["name"]);
        
        $num = I('post.ctnnum') * I('post.ctnper')  + I('post.fraction');
        $cost = $num * I('post.price');
        $_POST["cost"] = $cost;
        $_POST['createdate'] = date("Y-m-d H:i:s",time());
        

        //update [product] data
        $rows = $Table->where("id=$id")->select();
        A("product")->Inc($rows[0]['pid'], $rows[0]['count']);
        A("product")->Dec($rows[0]['pid'], $num);

        
        $result = $Table->where("id=$id")->save($_POST);
        if ($result) {
            echo json_encode ( array (
                    'success' => 'UPDATE successful.',
                    cost => $cost
            ) );
        } else {
            echo json_encode ( array (
                    'msg' => "Failed:".$Table->getLastSql(),
                    cost => $cost
            ) );
        }
    }
    
    public function Delete($id){
        $Table = M($this->__tname__);
        
        
        ///////////////////////////////////////////////////
        //Update table 'product'
        $rows = $Table->where("id=$id")->select();
        $pid = $rows[0]['pid'];
        $num = $rows[0]['count'];
        A("product")->Inc($pid, $num);
        
        
        $rs = $Table->where("id=$id")->delete();
        if ($rs) {
            echo json_encode ( array (
                    'success' => 'UPDATE successful.'
            ) );
        } else {
            echo json_encode ( array (
                    'msg' => mysql_error()
            ) );
        }
    }
    
}