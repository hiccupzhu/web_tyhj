<?php
namespace Admin\Controller;
use Think\Controller;

class TableController extends Controller {
    
    public $__tname__ = "";
    
    public function Create($tname=''){
        $tname = $this->__tname__ == "" ? $tname : $this->__tname__;
        
        $Table = M($tname);
        
        $result = $Table->create($_POST);
        
        
        if ($result && $Table->add()) {
            echo json_encode ( array (
                    'success' => 'SAVE successful.'
            ) );
            exit();
        }
        
        echo json_encode ( array (
                'msg' => mysql_error ()
        ) );
    }
   
    public function Retrieve($tname=''){
        $tname = $this->__tname__ == "" ? $tname : $this->__tname__;
        
        $Table = M($tname);
        
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
        $offset = ($page-1)*$rows;
        $result = array();
        
        $total = $Table->count();
        
        //dump($total);
        
        $result["total"] = $total;
        
        if(isset($_REQUEST['where'])){
            //这是为了避免以%传参时，若后面是数字，解析时将发生转义,此时用*代替%
            $where['_string'] = str_replace("*", "%",$_REQUEST['where']);
            $rs = $Table->where($where)->limit($offset, $rows)->select();
        }else{
            $rs = $Table->limit($offset, $rows)->select();
        }
        
        //dump($rs);
        $result["rows"] = $rs;
        
        echo json_encode($result);
    }
    
    
    
    public function Update($tname='', $id){
        $tname = $this->__tname__ == "" ? $tname : $this->__tname__;
        
        $Table = M($tname);
        
        $result = $Table->where("id=$id")->save($_POST);
        if ($result) {
            echo json_encode ( array (
                    'success' => 'UPDATE successful.'
            ) );
        } else {
            echo json_encode ( array (
                    'msg' => mysql_error()
            ) );
        }
    }
    
    public function Delete($tname='', $id){
        $tname = $this->__tname__ == "" ? $tname : $this->__tname__;
        
        $Table = M($tname);
        $rs = $Table->where("id=$id")->delete();
        
        if ($rs) {
            echo json_encode ( array (
                    'success' => 'UPDATE successful.'
            ) );
        } else {
            echo json_encode ( array (
                    'msg' => mysql_error()
            ) );
        }
    }
    
}