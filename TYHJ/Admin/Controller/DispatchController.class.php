<?php
namespace Admin\Controller;
use Think\Controller;

class DispatchController extends Controller {
    
    /*
     * did: dispatch id
     */
    public function FinishCache($did) {
        $cache = M('tmpdispch');
        
        $rows = $cache->select();
        
        $cache->startTrans();
        
        foreach ($rows as $row){
            unset($row['id']);
            
            A("product")->Dec($row['pid'], $row['count']);
        
            $Table = M('ostorage');
            
            $row['did'] = $did;
            $row['createdate'] = date("Y-m-d H:i:s",time());
            
            $result = $Table->data($row)->add();
            if($result == FALSE){
                $cache->rollback();
                exit(json_encode(array (
                	'msg' => 'Insert data ERROR:'.json_encode($row), 
                    'lastsql' => $Table->getLastSql()
                )));
            }
        }
        $cache->where('1=1')->delete();
        
        $Table->commit();
        
        $dispatch = M('dispatch');
        
        $data['did'] = $did;
        $data['userid'] = 0;
        $data['createdate'] = date("Y-m-d H:i:s",time());
        
        $dispatch->create($data);
        
        if ($result && $dispatch->add()) {
            exit(json_encode ( array (
                    'success' => 'SAVE successful.'
            ) ) );
        }else{
            echo json_encode ( array (
                    'msg' => $dispatch->getLastSql()
            ) );
        }
        
    }
    
}