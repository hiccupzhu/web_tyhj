<?php
/*
 * Tmpdispch: temp dispatch
 */
namespace Admin\Controller;


class TmpdispchController extends TableController {

    public $__tname__ = "tmpdispch";
    
    public function Retrieve(){
        //         $tname = "ostorage o";
        //下面的o是ostorage的一个别名，用在查询的时候
        $Table = M($this->__tname__." o");
    
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
        $offset = ($page-1)*$rows;
        $result = array();
    
        $total = $Table->count();
    
    
    
        $result["total"] = $total;
    
        $rs = $Table->join("t_product p on o.pid=p.id")->field("o.*,p.name")->limit($offset, $rows)->select();
    
        //dump($rs);
        $result["rows"] = $rs;
    
        echo json_encode($result);
    }
    
}