<?php
/*
 * Tmpdispch: temp dispatch
 */
namespace Admin\Controller;


class UserController extends TableController {

    public $__tname__ = "user";
    
    public function Retrieve(){
        //         $tname = "ostorage o";
        //下面的o是ostorage的一个别名，用在查询的时候
        $Table = M($this->__tname__);
    
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 10;
        $offset = ($page-1)*$rows;
        $result = array();
    
        $total = $Table->count();
    
        $result["total"] = $total;
    
        if(session("level") == 0){
            $rs = $Table->limit($offset, $rows)->select();
        }else{
            $rs = $Table->where("level > 0")->limit($offset, $rows)->select();
        }
    
        //dump($rs);
        $result["rows"] = $rs;
    
        echo json_encode($result);
    }
    
}