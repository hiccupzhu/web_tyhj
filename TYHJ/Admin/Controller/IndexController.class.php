<?php
namespace Admin\Controller;
use Think\Controller;
class IndexController extends Controller {
    protected function _initialize() {
        session_start();
        $username = session("name");
        
        if($username == ""){
            $this->redirect('Login/login');
        }
    }
    
//     public function _empty() {
//         header("HTTP/1.0 404 Not Found");
//         $this->display('/404');
//     }
    
    
    
    
    public function index(){
        $this->display();
    }
    
    public function warehouse(){
        $tname = "istorage";
        $url = "/index.php/table";

        $CURL = "/index.php/IStorage/Create";
        $DURL = "/index.php/IStorage/Delete";
        $RURL = "/index.php/IStorage/Retrieve";
        $UURL = "/index.php/IStorage/Update";
        
        $this->assign('CURL',$CURL);
        $this->assign('DURL',$DURL);
        $this->assign('RURL',$RURL);
        $this->assign('UURL',$UURL);
        
        
        
        $this->display();
    }
    
    
    public function delivery(){
        $url = "/index.php/OStorage";
        $CURL = $url."/Create";
        $DURL = $url."/Delete";
        $RURL = $url."/Retrieve";
        $UURL = $url."/Update";
    
        $this->assign('CURL',$CURL);
        $this->assign('DURL',$DURL);
        $this->assign('RURL',$RURL);
        $this->assign('UURL',$UURL);
    
    
    
        $this->display();
    }
    
    
    public function dispatch(){
        $url = "/index.php/Tmpdispch";
        $CURL = $url."/Create";
        $DURL = $url."/Delete";
        $RURL = $url."/Retrieve";
        $UURL = $url."/Update";
        
        $this->assign('CURL',$CURL);
        $this->assign('DURL',$DURL);
        $this->assign('RURL',$RURL);
        $this->assign('UURL',$UURL);
        
        
        $this->display();
    }
    
    
    public function product(){
        $tname = "product";
        $url = "/index.php/Table";
        $CURL = $url."/Create?tname=".$tname;
        $DURL = $url."/Delete?tname=".$tname;
        $RURL = $url."/Retrieve?tname=".$tname;
        $UURL = $url."/Update?tname=".$tname;
    
        $this->assign('CURL',$CURL);
        $this->assign('DURL',$DURL);
        $this->assign('RURL',$RURL);
        $this->assign('UURL',$UURL);
    
    
        $this->display();
    }
    
    
    public function storage(){
        $tname = "product";
        $url = "/index.php/table";
        $CURL = $url."/Create?tname=".$tname;
        $DURL = $url."/Delete?tname=".$tname;
        $RURL = $url."/Retrieve?tname=".$tname;
        $UURL = $url."/Update?tname=".$tname;
        
        $RURL = "/index.php/Product/Retrieve";
    
        $this->assign('CURL',$CURL);
        $this->assign('DURL',$DURL);
        $this->assign('RURL',$RURL);
        $this->assign('UURL',$UURL);
    
        $this->display();
    }
    
    
    public function user(){
        $tname = "user";
        $url = "/index.php/table";
        $CURL = $url."/Create?tname=".$tname;
        $DURL = $url."/Delete?tname=".$tname;
//         $RURL = $url."/Retrieve?tname=".$tname;
        $RURL = "/index.php/User/Retrieve";
        $UURL = $url."/Update?tname=".$tname;
    
        $this->assign('CURL',$CURL);
        $this->assign('DURL',$DURL);
        $this->assign('RURL',$RURL);
        $this->assign('UURL',$UURL);
    
    
    
        $this->display();
    }
    
}