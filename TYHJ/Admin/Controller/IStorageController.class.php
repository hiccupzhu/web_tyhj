<?php
namespace Admin\Controller;
use Think\Controller;

class IStorageController extends Controller {

    public function Create(){
        //箱数*每箱个数+散件数
        $num = I('post.ctnnum') * I('post.ctnper')  + I('post.fraction');
        
        A("product")->Inc(I('post.pid'), $num);
        
        $cost = $num * I('post.price');
        $_POST['cost'] = $cost;
        $_POST['createdate'] = date("Y-m-d H:i:s",time());
        
        
        $Table = M("istorage");
        $result = $Table->create($_POST);
        if ($result && $Table->add()) {
            echo json_encode ( array (
                    'success' => '保存成功',
                    cost => $cost
            ) );
            exit();
        }
        
        echo json_encode ( array (
                'msg' => mysql_error ()
        ) );
    }
   
    
    public function Retrieve(){
        $Model = M("istorage s");
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
        $offset = ($page-1)*$rows;
        $result = array();
        
        $rs = $Model->join("t_product p on s.pid=p.id")->field("s.*,p.name")->limit($offset, $rows)->select();
        
        $total = count($rs);
        
        $result["total"] = $total;
        //dump($rs);
        $result["rows"] = $rs;
        
        echo json_encode($result);
    }
    
    
    
    public function Update($id){
        unset($_POST["name"]);
        
        $istore = M("istorage");
        
        ///////////////////////////////////////////////////////////////////////
        //Calc and update [product]
        $num = I('post.ctnnum') * I('post.ctnper')  + I('post.fraction');
        $cost = $num * I('post.price');
        $_POST["cost"] = $cost;
        $_POST["count"]= $num;
        $_POST['createdate'] = date("Y-m-d H:i:s",time());
        
        //update [product] data
        $rows = $istore->where("id=$id")->select();
        A("product")->Dec($rows[0]['pid'], $rows[0]['count']);
        A("product")->Inc($rows[0]['pid'], $num);
        
//         echo "row_count:".$rows[0]['count']."   num:".$num;
        
        ///////////////////////////////////////////////////////////////////////
        //Update ['istorage']
        $result = $istore->where("id=$id")->save($_POST);
//         var_dump($result);
        if ($result == 0 || $result == true) {
            echo json_encode ( array (
                    'success' => '更新成功',
                    cost => $cost
            ) );
        } else {
            echo json_encode ( array (
                    'msg' => "Failed:".$istore->getDbError()
            ) );
        }
    }
    
    public function Delete($id){
        $istore = M("istorage");
        
        ///////////////////////////////////////////////////
        //Update table 'product'
        $rows = $istore->where("id=$id")->select();
        $pid = $rows[0]['pid'];
        $num = $rows[0]['count'];
        A("product")->Dec($pid, $num);
        
        
        //////////////////////////////////////////////////
        //Update table 'istorage'
        $rs = $istore->where("id=$id")->delete();
        if ($rs) {
            echo json_encode ( array (
                    'success' => 'UPDATE successful.'
            ) );
        } else {
            echo json_encode ( array (
                    'msg' => mysql_error()
            ) );
        }
    }
    
}