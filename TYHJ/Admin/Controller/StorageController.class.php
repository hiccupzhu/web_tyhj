<?php
namespace Admin\Controller;
use Think\Controller;

class StorageController extends Controller {

    public function Create($pid, $count){
        $Table = M("storage");
        
        $data['pid'] = $pid;
        $data['count'] = $count;
        
        $result = $Table->create($data);
        
        return $result;
    }
   
    
    public function Retrieve(){
        $Model = M("storage s");
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
        $offset = ($page-1)*$rows;
        $result = array();
        
        $rs = $Model->join("t_product p on s.pid=p.id")->field("s.*,p.name")->limit($offset, $rows)->select();
        
        $total = count($rs);
        
        $result["total"] = $total;
        //dump($rs);
        $result["rows"] = $rs;
        
        echo json_encode($result);
    }
    
    
    public function Update($id, $pid, $count){
        $Table = M("storage");
        
        $data['pid'] = $pid;
        $data['count'] = $count;
        
        $result = $Table->where("id=$id")->save($data);
        
        return $result;
    }
    
    public function Delete($id){
        $Table = A("Table");
        
        $Table->Delete("storage", $id);
    }
    
    
    public function Setup($pid, $count=0){
        $Table = M("storage");
        
        $rows = $Table->where("pid='$pid'")->count();
        $num = count($rows);
        
        //Have this product
        if( $num == 1) {
            $this->Update($rows[0]['id'], $pid, $count);
            
        }elseif($num == 0){
            $this->Create($pid, $count);
            
        }else{
            exit( json_encode(array(
            	msg => "数据库中存在相同的商品ID，请联系管理员！"
            )));
        }
    }
    
}