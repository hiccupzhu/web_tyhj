/*
Navicat MySQL Data Transfer

Source Server         : tyhj
Source Server Version : 50612
Source Host           : localhost:3306
Source Database       : tyhj

Target Server Type    : MYSQL
Target Server Version : 50612
File Encoding         : 65001

Date: 2015-08-30 22:32:32
*/
create database IF NOT EXISTS tyhj;

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_dispatch
-- ----------------------------
DROP TABLE IF EXISTS `t_dispatch`;
CREATE TABLE `t_dispatch` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `did` char(16) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `userid` bigint(20) DEFAULT NULL,
  `status` char(16) DEFAULT NULL,
  `finishdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_dispatch
-- ----------------------------
INSERT INTO `t_dispatch` VALUES ('1', '0120150829181845', '2015-08-29 18:26:56', '0', null, null);

-- ----------------------------
-- Table structure for t_istorage
-- ----------------------------
DROP TABLE IF EXISTS `t_istorage`;
CREATE TABLE `t_istorage` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pid` char(32) NOT NULL,
  `ctnper` int(10) unsigned DEFAULT '0',
  `ctnnum` int(10) unsigned DEFAULT '0',
  `fraction` int(10) unsigned DEFAULT '0',
  `price` double DEFAULT '0',
  `cost` double DEFAULT '0',
  `count` int(10) DEFAULT '0',
  `createdate` char(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_istorage
-- ----------------------------
INSERT INTO `t_istorage` VALUES ('1', '6906907401022', '24', '100', '1', '2.1', '5042.1', '2401', null);
INSERT INTO `t_istorage` VALUES ('2', '6906907401022', '24', '100', '10', '1.89', '4554.9', '2410', null);
INSERT INTO `t_istorage` VALUES ('3', '6906907403088', '24', '50', '2', '1.77', '2127.54', '1202', null);
INSERT INTO `t_istorage` VALUES ('4', '6906907403088', '24', '50', '1', '1.77', '2125.77', '1201', null);
INSERT INTO `t_istorage` VALUES ('5', '6913869012111', '20', '2', '12', '20', '1040', '52', null);
INSERT INTO `t_istorage` VALUES ('6', '6920354800399', '24', '50', '0', '11', '13200', '1200', null);

-- ----------------------------
-- Table structure for t_ostorage
-- ----------------------------
DROP TABLE IF EXISTS `t_ostorage`;
CREATE TABLE `t_ostorage` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pid` char(32) NOT NULL,
  `did` char(16) DEFAULT NULL,
  `ctnper` int(10) unsigned DEFAULT '0',
  `ctnnum` int(10) unsigned DEFAULT '0',
  `fraction` int(10) unsigned DEFAULT '0',
  `price` double DEFAULT '0',
  `cost` double DEFAULT '0',
  `count` int(10) DEFAULT '0',
  `userid` bigint(20) DEFAULT NULL,
  `createdate` char(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_ostorage
-- ----------------------------
INSERT INTO `t_ostorage` VALUES ('1', '6906907401022', '', '24', '100', '12', '2.2', '5306.4', '2412', null, '2015-08-24 22:26:31');
INSERT INTO `t_ostorage` VALUES ('4', '6920354800399', '', '24', '0', '13', '2', '26', '13', null, '2015-08-26 21:53:17');
INSERT INTO `t_ostorage` VALUES ('7', '6913869012111', '', '12', '1', '2', '2', '28', '14', null, '2015-08-24 22:47:41');
INSERT INTO `t_ostorage` VALUES ('8', '6913869012111', '0120150829181845', '12', '1', '2', '2', '28', '14', null, '2015-08-24 22:47:41');

-- ----------------------------
-- Table structure for t_product
-- ----------------------------
DROP TABLE IF EXISTS `t_product`;
CREATE TABLE `t_product` (
  `id` char(32) NOT NULL,
  `name` char(255) NOT NULL,
  `count` int(10) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_product
-- ----------------------------
INSERT INTO `t_product` VALUES ('6906907401022', '百事可乐', '4811');
INSERT INTO `t_product` VALUES ('6906907403088', '美年达-橙味', '2403');
INSERT INTO `t_product` VALUES ('6913869012111', '珠芽蓼止泻颗粒', '24');
INSERT INTO `t_product` VALUES ('6920354800399', '高露洁牙膏 美白健齿', '1187');

-- ----------------------------
-- Table structure for t_tmpdispch
-- ----------------------------
DROP TABLE IF EXISTS `t_tmpdispch`;
CREATE TABLE `t_tmpdispch` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pid` char(32) NOT NULL,
  `did` char(16) DEFAULT NULL,
  `ctnper` int(10) unsigned DEFAULT '0',
  `ctnnum` int(10) unsigned DEFAULT '0',
  `fraction` int(10) unsigned DEFAULT '0',
  `price` double DEFAULT '0',
  `cost` double DEFAULT '0',
  `count` int(10) DEFAULT '0',
  `userid` bigint(20) DEFAULT NULL,
  `createdate` char(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_tmpdispch
-- ----------------------------

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(64) NOT NULL,
  `fullname` char(64) DEFAULT NULL,
  `password` char(32) NOT NULL,
  `phone` char(32) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `level` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'admin', '张三', '', '18911116666', '', '0');
INSERT INTO `t_user` VALUES ('2', 'test1', 'test1', '', '1891111111', '', '0');
